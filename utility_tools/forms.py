from django import forms


class BaseModelForm(forms.ModelForm):
    form_css_class: str = 'form-control'
    readonly_fields: tuple = ()

    def __init__(self, *args, **kwargs):
        super(BaseModelForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields.get(field).widget.attrs.update({'class': self.form_css_class})
            if field in self.readonly_fields:
                self.fields.get(field).disabled = True
