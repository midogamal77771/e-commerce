from django.shortcuts import render, reverse
from django.http.response import JsonResponse

from order.models import OrderItem
from .decorators import required_ajax, required_admin_user


@required_admin_user
def charts(request):
    context = {
        'monthly_sales_url': reverse('data_processing:monthly_sales'),
        'sub_category_sales_url': reverse('data_processing:sub_category_sales'),
        'main_category_sales_url': reverse('data_processing:main_category_sales')
    }
    return render(request, 'data_processing/charts.html', context)


@required_ajax(method='GET')
def get_monthly_sales(Request):
    data = list(OrderItem.objects.group_by('order__created__date', 'order__created__date'))
    new_data = list(OrderItem.objects.return_group_by('order__created__date', 'order__created__date'))
    return JsonResponse(data={'sales': data, 'returns': new_data}, safe=False)


@required_ajax(method='GET')
def get_sub_category_sales(request):
    data = list(OrderItem.objects.group_by('product__sub_category__name', 'total_price'))
    return JsonResponse(data=data, safe=False)


@required_ajax(method='GET')
def get_main_category_sales(request):
    data = list(OrderItem.objects.group_by('product__sub_category__main_category__name', 'total_price'))
    return JsonResponse(data=data, safe=False)
