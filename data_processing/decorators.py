from typing import Callable
from functools import wraps, partial

from django.core.exceptions import PermissionDenied
from django.http.response import HttpResponseBadRequest


def required_ajax(view_func: Callable = None, method: str = None):
    valid_methods = ('GET', 'POST', 'PUT', 'DELETE')

    if view_func is None:
        return partial(required_ajax, method=method)

    if method not in valid_methods:
        raise ValueError(f'Invalid method, it should be one of ({", ".join(valid_methods)})')

    @wraps(view_func)
    def wrapper_func(request, *args, **kwargs):
        if request.is_ajax() and request.method == method:
            return view_func(request, *args, **kwargs)
        return HttpResponseBadRequest(f'Invalid request type - should be ajax -, or method - should be one of ({", ".join(valid_methods)}) -')

    return wrapper_func


def required_admin_user(view_func=None, redirect_url: str = None, message: str = None):

    if view_func is None:
        return partial(required_admin_user, redirect_url, message)

    message = message or 'Admin users only'

    @wraps(view_func)
    def wrapper_func(request, *args, **kwargs):
        if request.user.is_authenticated and request.user.is_staff:
            return view_func(request, *args, **kwargs)
        return PermissionDenied(message)

    return wrapper_func
