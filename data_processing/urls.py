from django.urls import path

from .views import charts, get_monthly_sales, get_sub_category_sales, get_main_category_sales


app_name = 'data_processing'

urlpatterns = [
    path('charts', charts, name='charts'),
    path('monthly_sales', get_monthly_sales, name='monthly_sales'),
    path('sub_category_sales', get_sub_category_sales, name='sub_category_sales'),
    path('main_category_sales', get_main_category_sales, name='main_category_sales'),
]
