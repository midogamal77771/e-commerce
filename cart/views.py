from django.contrib import messages
from django.shortcuts import render, redirect

from .cart import Cart


def cart_add(request, id):
    Cart(request).add(str(id))
    return redirect('cart:cart_detail')


def cart_remove(request, id):
    Cart(request).remove(str(id))
    return redirect('cart:cart_detail')


def cart_delete_all(request):
    Cart(request).clear()
    return redirect('shop:home')


def cart_update_quantity(request, id):
    quantity = request.GET.get('quantity')
    if not quantity.isnumeric():
        raise ValueError('Invalid Quantity number, it must be numeric')
    Cart(request).add(id, quantity)
    return redirect('cart:cart_detail')


def cart_detail(request):
    cart = Cart(request)
    if len(cart) == 0:
        messages.info(request, 'Cart list is empty !! Fill it first to show ')
        return redirect('shop:home')
    return render(request, 'cart/detail.html')
