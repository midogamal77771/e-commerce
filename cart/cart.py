from django.db.models import Q
from django.conf import settings
from django.forms import model_to_dict
from django.shortcuts import get_object_or_404

from shop.models import Product
from .models import CartModel, CartItem


def validate__positive_numeric_value(num):
    return bool(str(num).isnumeric() and int(num) > 0)


class Cart:
    session_id = settings.CART_SESSION_ID

    def __init__(self, request):
        self.session = request.session
        cart = self.session.get(self.session_id)

        if not cart:
            cart = self.session[self.session_id] = {}
        self.cart = cart

        if not self.cart.get('products', None):
            self.cart['products'] = {}

        self.cart_model = None
        if request.user.is_authenticated:
            self.cart_model, created = CartModel.objects.get_or_create(user=request.user)
            if not created:
                items = self.get_cart_products()
                self.cart['products'] = {item.product.id: item.quantity for item in items}

    def __len__(self):
        return len(self.cart.get('products'))

    def __iter__(self):
        products = self.get_proper_products()
        yield from products

    def add(self, product_id, quantity=1):
        if not validate__positive_numeric_value(quantity):
            raise ValueError('value error for product quantity, mut be positive integer number')

        product = get_object_or_404(Product, id=product_id)

        if self.cart_model:
            CartItem.objects.get_or_create(cart=self.cart_model, product=product, quantity=quantity)

        # if product_id not in self.cart['products']:
        self.cart['products'][product_id] = quantity
        self.save()

    def remove(self, product_id):

        if self.cart_model:
            CartItem.objects.filter(product__id=product_id).delete()

        if product_id in self.cart['products']:
            self.cart['products'].pop(product_id)
            self.save()

    def save(self):
        self.session[self.session_id] = self.cart
        self.session.modified = True

    def clear(self):
        self.session[self.session_id] = {}
        self.session.modified = True
        if self.cart_model:
            CartItem.objects.filter(cart=self.cart_model).delete()

    def update_quantity(self, product_id, quantity):
        if not validate__positive_numeric_value(quantity):
            raise ValueError('value error for product quantity, mut be positive integer number')

        if self.cart_model:
            CartItem.objects.update_or_create(cart=self.cart_model, product__id=product_id,
                                              defaults={'quantity': quantity})
        print( self.cart.get('products'))
        self.cart.get('products').update({product_id: quantity})
        self.save()
        print( self.cart.get('products'))
        print(len(self))

    def get_cart_products(self):
        if self.cart_model:
            return self.cart_model.items.exclude(Q(cart__isnull=True) | Q(product__isnull=True))
        return Product.objects.filter(id__in=self.get_cart_content().keys())

    def get_proper_products(self):
        items = self.get_cart_products()
        if isinstance(items.first(), CartItem):
            return items
        lst = []
        for product in items:
            product_dict = model_to_dict(product)
            quantity = self.cart.get("products")[str(product.id)]
            cost = product.get_price() * int(quantity)
            lst.append({'product': product_dict,  'quantity': quantity,  'get_cost': cost})
        return lst

    def get_cart_content(self):
        return self.cart.get('products')

    def get_total_price(self):
        return sum(i.get_cost() if hasattr(i, 'get_cost') and callable(getattr(i, 'get_cost')) else i.get('get_cost')
                   for i in self.get_proper_products())
