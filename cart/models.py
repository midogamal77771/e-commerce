from django.db import models
from django.contrib.auth.backends import get_user_model

from datetime import datetime

from shop.models import Product


User = get_user_model()


class CartModel(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=datetime.now)


class CartItem(models.Model):
    cart = models.ForeignKey(CartModel, on_delete=models.CASCADE, related_name='items', null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    quantity = models.PositiveIntegerField(default=1)

    def get_cost(self):
        return self.product.get_price() * self.quantity
