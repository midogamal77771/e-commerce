from django.contrib import messages
from django.shortcuts import redirect

from .cart import Cart


class CartLenMixIn:
    def dispatch(self, request, *args, **kwargs):
        cart = Cart(request)
        if len(cart) > 0:
            return super().dispatch(request, *args, **kwargs)
        messages.error(request, "Cart is empty !!")
        return redirect('shop:home')

