from django.urls import path
from .views import cart_detail, cart_add, cart_remove, cart_update_quantity, cart_delete_all

app_name = 'cart'

urlpatterns = [
    path('detail/', cart_detail, name='cart_detail'),
    path('add/<int:id>/', cart_add, name='cart_add'),
    path('remove/<int:id>/', cart_remove, name='cart_remove'),
    path('update_quantity/<int:id>/', cart_update_quantity, name='cart_update_quantity'),
    path('delete_all/', cart_delete_all, name='cart_delete_all'),
]
