from django import forms

import django_filters as fil

from .models import Product, SubCategory, MainCategory


class ProductFilter(fil.FilterSet):
    name = fil.CharFilter(label='Product Name', field_name="name", lookup_expr="icontains",
                          widget=forms.TextInput(attrs={'class': 'form-control'}))
    description = fil.CharFilter(field_name="short_description", lookup_expr="icontains",
                                 widget=forms.TextInput(attrs={'class': 'form-control'}))
    main_category = fil.ModelChoiceFilter(field_name='sub_category__main_category', label='Main category',
                                          queryset=MainCategory.objects.all(),
                                          widget=forms.Select(attrs={'class': 'form-control'}))
    category = fil.ModelChoiceFilter(field_name='sub_category', label='Sub category',
                                     queryset=SubCategory.objects.all(),
                                     widget=forms.Select(attrs={'class': 'form-control'}))
    lower_limit = fil.NumberFilter(field_name="price", lookup_expr="lte",
                                   widget=forms.NumberInput(attrs={'class': 'form-control'}))
    upper_limit = fil.NumberFilter(field_name="price", lookup_expr="gte",
                                   widget=forms.NumberInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Product
        fields = ("name", )
