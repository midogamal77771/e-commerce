from django.views.generic import DetailView, ListView, TemplateView

from django_filters.views import FilterView

from .models import Product
from .forms import ProductFilter
from info.models import CarouselImage


class HomePage(TemplateView):
    template_name = 'home.html'
    extra_context = {
        'in_sale_products': Product.objects.in_sale(),
        'recent_products': Product.objects.recent_add(),
        'best_seller_products': Product.objects.best_sales(),
        'carousel_images': CarouselImage.objects.all()
    }


class ProductListView(FilterView):
    model = Product
    filterset_class = ProductFilter
    template_name = 'shop/product_list.html'


class ProductDetailView(DetailView):
    model = Product
    context_object_name = 'product_obj'
    template_name = 'shop/product_detail.html'

    def get_context_data(self, **kwarg):
        context = super(ProductDetailView, self).get_context_data(**kwarg)
        context.update({'related_products': Product.objects.filter(sub_category=self.object.sub_category)})
        return context


class ProductCategoryListView(ListView):
    model = Product
    template_name = 'shop/product_category_list.html'
    context_object_name = 'products'

    def get_queryset(self):
        return self.model.objects.filter(sub_category__id=self.kwargs.get('pk'))
