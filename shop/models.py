from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator

from ckeditor.fields import RichTextField

from .managers import ProductManager
from utility_tools.decorators import decimal_limit


class MainCategory(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Main Categories'
        verbose_name_plural = 'Main Categories'


class SubCategory(models.Model):
    name = models.CharField(max_length=120)
    main_category = models.ForeignKey(MainCategory, on_delete=models.SET_NULL, null=True, related_name='sub_categories')

    def get_absolute_url(self):
        return reverse('shop:product_category_list', args=[self.pk])

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Sub Categories'
        verbose_name_plural = 'Sub Categories'


class Product(models.Model):
    sub_category = models.ForeignKey(SubCategory, on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=200, db_index=True, unique=True)
    image = models.ImageField(upload_to="products/")
    short_description = models.CharField(max_length=250, null=True)
    long_description = RichTextField(null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(1), ])
    price_sale = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True, validators=[MinValueValidator(1), ])
    trader_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True, validators=[MinValueValidator(1), ])
    stock = models.PositiveIntegerField(validators=[MinValueValidator(1), ])
    sold = models.PositiveIntegerField(null=True, default=0)
    available = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    objects = ProductManager()

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Products'
        verbose_name_plural = 'Products'

    def __str__(self):
        return self.name

    def is_available(self):
        return self.stock > 0 and self.available

    def in_sale(self):
        return bool(self.price_sale)

    def is_new(self):
        return self.get_days_of_creation() < 30

    def clean(self):
        if self.price_sale and self.price_sale > self.price:
            raise ValidationError('Price sale must be less than price')
        if self.trader_price and (self.trader_price > self.price or self.trader_price > self.price_sale):
            raise ValidationError('Trader price should be less than price and sale')

    def get_absolute_url(self):
        return reverse('shop:product_detail', args=[self.pk])

    @decimal_limit
    def sale_percentage(self):
        if self.price_sale and self.price_sale < self.price and self.price_sale != 0:
            return 100*((self.price - self.price_sale)/self.price)
        return 0

    def get_days_of_creation(self):
        return (timezone.now() - self.created).days

    def get_price(self):
        return self.price_sale or self.price


class ProductImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField()
