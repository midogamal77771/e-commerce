from django.db import models


class ProductManager(models.Manager):

    def available(self):
        return self.filter(available=True, stock__gt=0)

    def in_sale(self): return self.filter(
        price_sale__isnull=False,
        price_sale__lte=models.F('price'),
    )

    def recent_add(self, days=30):
        return self.filter(created__day__lte=days)

    def best_sales(self):
        return self.filter().order_by('sold')
