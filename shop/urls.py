from django.urls import path
from .views import HomePage, ProductListView, ProductDetailView, ProductCategoryListView


app_name = 'shop'

urlpatterns = [
    path('', HomePage.as_view(), name='home'),
    path('product_list/', ProductListView.as_view(), name='product_list'),
    path('product_detail/<int:pk>/', ProductDetailView.as_view(), name='product_detail'),
    path('product_category_list/<int:pk>/', ProductCategoryListView.as_view(), name='product_category_list'),
]
