from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import MainCategory, SubCategory, Product, ProductImage


class SubCategoryInlineAdmin(admin.TabularInline):
    model = SubCategory
    min_num = 0
    extra = 5


class ImageInlineAdmin(admin.TabularInline):
    model = ProductImage
    min_num = 1
    readonly_fields = ['show_image']

    def show_image(self, obj):
        return mark_safe(f"<a href='{obj.image.url}' ><img src='{obj.image.url}' width={150} height={200}></a>")

    show_image.short_description = 'Images'


class ProductAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Product Main Info', {'fields': (
            ('sub_category', ),
            ('name', ),
            'short_description', 'long_description',
            ('image', 'show_form_image'),
            ('price', 'price_sale', 'trader_price'),
            ('available', 'stock'),
            'created',
        )}),
    )
    list_display = ['name', 'sub_category', 'price', 'stock', 'available', 'updated', 'get_days_of_creation', 'show_list_image']
    list_filter = ['available', 'created', 'updated', 'sub_category']
    list_editable = ['available', ]
    search_fields = ['name']
    prepopulated_field = {'slug': ('name',)}
    actions = ['out_of_stack', 'in_our_stack']
    readonly_fields = ['show_form_image', 'created', ]
    inlines = (ImageInlineAdmin, )

    def available_update(self, request, queryset):
        query = queryset.update(available=False)
        self.message_user(request, f'{query.count} has changed successfully')

    def unavailable_update(self, request, queryset):
        query = queryset.update(available=True)
        self.message_user(request, f'{query.count} has changed successfully')

    def main_category(self, instance):
        return instance.category.main_category.name

    def show_form_image(self, instance):
        url = instance.image.url
        return mark_safe(f'<a href="{url}"> <img src="{url}" width="{250}" height={150} /></a>')

    def show_list_image(self, instance):
        url = instance.image.url
        return mark_safe(f'<a href="{url}"> <img src="{url}" width="{150}" height={100} /></a>')

    show_form_image.short_description = ''
    show_list_image.short_description = 'Image'
    main_category.short_description = 'main category name'
    available_update.short_description = 'make it available'
    unavailable_update.short_description = 'make it unavailable'


class MainCategoryAdmin(admin.ModelAdmin):
    inlines = [SubCategoryInlineAdmin, ]


class SubCategoryAdmin(admin.ModelAdmin):
    fields = ('main_category', 'name')


admin.site.register(SubCategory, SubCategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(MainCategory, MainCategoryAdmin)

# admin.site_site_title = 'Trio Shop'
# admin.site.site_header = 'Trio'
# admin.site.index_title = 'Trio Admin'
# admin.site.site_url = reverse_lazy('shop:home')
