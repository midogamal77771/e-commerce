from .models import MainCategory


def category_context(request):
    return {'main_categories': [{'name': main_cat.name, 'sub_categories': [
        {'name': sub_cat.name, 'link': sub_cat.get_absolute_url()} for sub_cat in main_cat.sub_categories.all()[:6]
    ]} for main_cat in MainCategory.objects.all()[:4]]}
