from .models import Product


def validate_product_quantity(product_id, quantity=1) -> bool:
    product = Product.objects.filter(id=product_id)
    if not product.exists():
        raise ValueError('')
    return product.first().stock >= quantity
