from django.contrib import messages
from django.shortcuts import render, redirect

from .wish import Wish


def wish_add(request, id):
    wish = Wish(request)
    wish.add(str(id))
    return redirect('wish:wish_detail')


def wish_remove(request, id):
    Wish(request).remove(str(id))
    return redirect('wish:wish_detail')


def wish_delete_all(request):
    Wish(request).clear()
    return redirect('shop:home')


def wish_detail(request):
    wish = Wish(request)
    if len(wish) == 0:
        messages.info(request, 'Wish list is empty !! Fill it first to show ')
        return redirect('shop:home')
    return render(request, 'wish/wish.html')


