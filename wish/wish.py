from django.conf import settings

from .models import WishModel
from shop.models import Product


def product_validation(product_id):
    product = Product.objects.filter(id=product_id)
    if product.exists():
        product = product.first()
        return product
    raise ValueError('value error for product id')


class Wish:

    def __init__(self, request):
        self.session = request.session
        wish = self.session.get(settings.WISH_SESSION_ID)

        if not wish:
            wish = self.session[settings.WISH_SESSION_ID] = {}
        self.wish = wish

        if not self.wish.get('products', None):
            self.wish['products'] = []

        self.wish_model = None
        if request.user.is_authenticated:
            self.wish_model, created = WishModel.objects.get_or_create(user=request.user)
            if not created:
                self.wish['products'] = [i.id for i in self.wish_model.products.all()]

    def __len__(self):
        return len(self.wish['products'])

    def __iter__(self):
        for i in self.get_products():
            yield i

    def add(self, product_id):
        product = product_validation(product_id)

        if self.wish_model and product not in self.wish_model.products.all():
            self.wish_model.products.add(product)

        if product_id not in self.wish['products']:
            self.wish['products'].append(product_id)
            self.save()

    def remove(self, product_id):
        product = product_validation(product_id)

        if self.wish_model and product in self.wish_model.products.all():
            self.wish_model.products.remove(product)

        if product_id in self.wish['products']:
            self.wish['products'].remove(product_id)
            self.save()

    def save(self):
        self.session[settings.WISH_SESSION_ID] = self.wish
        self.session.modified = True

    def clear(self):
        self.session[settings.WISH_SESSION_ID] = {}
        self.session.modified = True
        if self.wish_model:
            self.wish_model.products.clear()

    def get_products(self):
        return Product.objects.filter(id__in=self.wish['products'])
