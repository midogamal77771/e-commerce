from django.db import models
from django.contrib.auth.backends import get_user_model

from shop.models import Product


User = get_user_model()


class WishModel(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    products = models.ManyToManyField(Product)
