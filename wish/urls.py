from django.urls import path
from .views import wish_detail, wish_add, wish_remove, wish_delete_all


app_name = 'wish'


urlpatterns = [
    path('detail/', wish_detail, name='wish_detail'),
    path('add/<int:id>/', wish_add, name='wish_add'),
    path('remove/<int:id>/', wish_remove, name='wish_remove'),
    path('delete_all/', wish_delete_all, name='wish_delete_all'),
]
