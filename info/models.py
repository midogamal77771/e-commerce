from django.db import models
from django.utils.translation import gettext as _
from django.core.validators import RegexValidator

from ckeditor.fields import RichTextField


PhoneNumberValidator = RegexValidator(r'^[0-9]{12,14}$', _('You entre invalid number'))


class CarouselImage(models.Model):
    image = models.ImageField(upload_to='carousel/')
    text = models.CharField(max_length=550, null=True, blank=True)

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Carousel Images'
        verbose_name_plural = 'Carousel Images'


class Contact(models.Model):
    address = models.TextField(_('Address'))
    email = models.EmailField(_('Email'))
    phone = models.CharField(_('Phone Number'), max_length=20, validators=[PhoneNumberValidator, ])
    linkedin = models.URLField(_('LinkedIn link'))
    facebook = models.URLField(_('Facebook link'))
    instagram = models.URLField(_('Instagram link'))
    twitter = models.URLField(_('Twitter link'))
    youtube = models.URLField(_('YouTube link'))
    whatsapp = models.CharField(_('Whatsapp number'), max_length=20, validators=[PhoneNumberValidator, ])

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Contact Info'
        verbose_name_plural = 'Contact Info'


class ContactUsMessage(models.Model):
    full_name = models.CharField(max_length=150)
    business_name = models.CharField(max_length=150)
    email = models.EmailField()
    phone = models.CharField(max_length=15, validators=(PhoneNumberValidator, ))
    country = models.CharField(max_length=150)
    city = models.CharField(max_length=150)
    subject = models.CharField(max_length=150)
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.full_name

    class Meta:
        verbose_name = 'Contact Us Messages'
        verbose_name_plural = 'Contact Us Messages'


class CategoryQuestion(models.Model):
    name = models.CharField(max_length=450)

    def __str__(self):
        return self.name


class Question(models.Model):
    category = models.ForeignKey(CategoryQuestion, on_delete=models.SET_NULL, null=True, related_name='questions')
    question = models.CharField(max_length=1500)
    answer = RichTextField(null=True)

    def __str__(self):
        return self.question
