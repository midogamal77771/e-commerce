from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Contact, ContactUsMessage, CarouselImage, CategoryQuestion, Question


class NOAddDeletePermission(admin.ModelAdmin):
    max_add: int = 1

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        if self.model.objects.count() >= self.max_add:
            return False
        return super().has_add_permission(request)


class ContactAdmin(NOAddDeletePermission):
    list_display = ('email', 'phone', 'whatsapp', 'show_links')
    readonly_fields = ('show_links', )

    def show_links(self, instance):
        return mark_safe(f"""
            <a href="{instance.facebook}" class="px-2"><i class="fab fa-facebook"></i></a>
            <a href="{instance.instagram}" class="px-2"><i class="fab fa-instagram"></i></a>
            <a href="{instance.twitter}" class="px-2"><i class="fab fa-twitter"></i></a>
            <a href="{instance.linkedin}" class="px-2"><i class="fab fa-linkedin"></i></a>
            <a href="{instance.youtube}" class="px-2"><i class="fab fa-youtube"></i></a>
            <a href="https://wa.me/+{instance.whatsapp}" class="px-2"><i class="fab fa-whatsapp"></i></a>
        """) if instance else "---"

    show_links.short_description = 'Links'


class ContactUsMessageAdmin(NOAddDeletePermission):
    list_display = ('full_name', 'business_name', 'email', 'phone', 'subject', 'timestamp')
    ordering = ('-timestamp', )


class CarouselImageAdmin(admin.ModelAdmin):
    list_display = ['text', 'show_image']

    def show_image(self, obj):
        return mark_safe(f"<a href='{obj.image.url}' ><img src='{obj.image.url}' width={150} height={200}></a>")

    show_image.short_description = 'Image'


admin.site.register(Question)
admin.site.register(CategoryQuestion)
admin.site.register(Contact, ContactAdmin)
admin.site.register(CarouselImage, CarouselImageAdmin)
admin.site.register(ContactUsMessage, ContactUsMessageAdmin)
