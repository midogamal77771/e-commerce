from django.urls import path
from .views import ContactUsMessageView, QuestionListView


app_name = 'info'

urlpatterns = [
    path('contact-us/', ContactUsMessageView.as_view(), name='contact_us'),
    path('Q&A/', QuestionListView.as_view(), name='question')
]
