from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView
from django.contrib.messages.views import SuccessMessageMixin

from .forms import ContactUsMessageForm
from .models import ContactUsMessage, Contact, CategoryQuestion


class ContactUsMessageView(CreateView, SuccessMessageMixin):
    model = ContactUsMessage
    form_class = ContactUsMessageForm
    template_name = 'info/contact_us.html'
    success_message = 'Message has been saved successfully'
    success_url = reverse_lazy('shop:home')
    extra_context = {
        'contact_info': Contact.objects.first()
    }


class QuestionListView(ListView):
    model = CategoryQuestion
    context_object_name = 'category_questions'
    template_name = 'info/question.html'
