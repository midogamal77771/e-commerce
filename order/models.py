from decimal import Decimal

from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.core.validators import ValidationError
from django.contrib.auth.backends import get_user_model

from shop.models import Product
from .managers import OrderItemManger


User = get_user_model()


OrderStatus: tuple = (
    ('placed', 'Placed'),
    ('processed', 'Processed'),
    ('shipped', 'Shipped'),
    ('delivered', 'Delivered'),
)

PaymentTypes: tuple = (
    ('cash', 'Cash'),
    ('visa', 'Visa'),
    ('installment', 'Installment')
)

MaxReturnDays: int = 15
RangeReturnDaysDiscount: tuple = 16, 30


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    postal_code = models.CharField(max_length=20, null=True, blank=True)
    location = models.CharField(max_length=150)
    more_description = models.TextField(null=True)

    payment_method = models.CharField(choices=PaymentTypes, max_length=150, null=True)
    status = models.CharField(choices=OrderStatus, max_length=120, null=True, default='placed')

    shipping_charge = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True, default=0.00)
    delivered = models.BooleanField(default=False)
    delivered_date = models.DateTimeField(null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-created',)

    def get_all_items(self):
        return self.items.all()

    def get_proper_items(self):
        return self.get_all_items().filter(item_return__isnull=True)

    def get_returned_items(self):
        return self.get_all_items().exclude(self.get_proper_items())

    def get_total_items_cost(self):
        return sum(item.get_total_item_cost() for item in self.get_proper_items())

    def get_total_order_cost(self):
        total = self.get_total_items_cost()
        if self.shipping_charge:
            total += self.shipping_charge
        return total
    get_total_order_cost.short_description = 'Total order cost'

    def get_absolute_url(self):
        return reverse('order:order_detail', args=[self.pk])

    def get_items_url(self):
        return reverse('order:order_items_update', args=[self.pk])

    def available_by_status(self):
        return self.status in ('placed', 'processed')

    def available_by_date(self):
        difference = timezone.now() - self.created
        return difference.days < MaxReturnDays

    def is_delivered(self):
        return self.status == 'delivered'


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items', on_delete=models.CASCADE, null=True)
    product = models.ForeignKey(Product, related_name='order_items', on_delete=models.SET_NULL, null=True)
    quantity = models.PositiveIntegerField(default=1)
    created = models.DateTimeField(auto_now_add=True, null=True)
    updated = models.DateTimeField(auto_now=True, null=True)

    objects = OrderItemManger()

    def __str__(self):
        return self.product.name

    def get_total_item_cost(self):
        return self.quantity * self.product.get_price()

    def get_product_name(self):
        return self.product.name

    def available_for_return(self):
        return self.order.available_by_date() and self.order.is_delivered()

    def get_status(self):
        if OrderItemReturn.objects.filter(order_item=self):
            return 'returned'
        return self.order.status


class OrderItemReturn(models.Model):
    order_item = models.OneToOneField(OrderItem, on_delete=models.CASCADE, null=True, related_name='item_return')
    extra_fees = models.DecimalField(decimal_places=2, max_digits=10, null=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.order_item.product.name


class Installment(models.Model):
    order = models.OneToOneField(Order, on_delete=models.CASCADE, related_name='installment')
    number_of_months = models.PositiveIntegerField()
    month_pay_amount = models.DecimalField(null=True, decimal_places=4, max_digits=12)

    def get_proper_installment_pay(self):
        return self.installment_pay.all()

    def get_installment_pay_count(self):
        return self.get_proper_installment_pay().count()

    def get_reminder_installment_pay_count(self):
        return self.number_of_months - self.get_installment_pay_count()

    def get_total_installment_pay(self):
        return sum(i.pay_amount for i in self.get_proper_installment_pay())

    def get_total_cost(self):
        return self.number_of_months * self.month_pay_amount


class InstallmentPay(models.Model):
    installment = models.ForeignKey(Installment, on_delete=models.CASCADE, related_name='installment_pay')
    month_number = models.PositiveIntegerField()
    pay_amount = models.PositiveIntegerField()

    def save(self, *args, **kwargs):
        if self.pay_amount != self.installment.month_pay_amount:
            raise ValidationError(f'Pay amount is not same as needed: {self.installment.month_pay_amount}')
        super(InstallmentPay, self).save(*args, **kwargs)


@receiver(post_save, sender=OrderItem)
def update_product_stock(sender, instance, created, **kwargs):
    if created:
        quantity = instance.quantity
        product_id = instance.product.id
        Product.objects.filter(id=product_id).update(stock=models.F('stock') - quantity,
                                                     sold=models.F('sold') + quantity)


@receiver(post_save, sender=OrderItemReturn)
def extra_fees_handling(sender, instance, created, **kwargs):
    if created:
        difference = instance.order_item.order.created - instance.date
        price = instance.order_item.product.get_price()
        new_price = price * (15/100) if difference.days in range(*RangeReturnDaysDiscount) else 0
        instance.extra_fees = new_price
        instance.save()

        product_id = instance.order_item.product.id
        quantity = instance.order_item.quantity
        Product.objects.filter(id=product_id).update(stock=models.F('stock') - quantity,
                                                     sold=models.F('sold') + quantity)


@receiver(post_save, sender=Installment)
def interest_rate_handling(sender, instance, created, **kwargs):
    if created:
        one_month_interest_rate = Decimal((20/100) / 12)
        total_months_interest_rate = instance.number_of_months * one_month_interest_rate

        order_cost = instance.order.get_total_items_cost()
        total_interest_rate = order_cost * total_months_interest_rate
        total_order_cost = order_cost + total_interest_rate

        instance.month_pay_amount = total_order_cost / instance.number_of_months
        instance.save()
