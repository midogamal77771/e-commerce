from django.urls import path
from .views import (OrderCreationView, OrderDetailView, OrderListView, OrderUpdateView, OrderItemInlineView,
                    OrderItemReturnView, InstallmentCreationView)


app_name = 'order'

urlpatterns = [
    path('list/', OrderListView.as_view(), name='order_list'),
    path('creation/', OrderCreationView.as_view(), name='order_creation'),
    path('detail/<int:pk>/', OrderDetailView.as_view(), name='order_detail'),
    path('update/<int:pk>/', OrderUpdateView.as_view(), name='order_update'),
    path('items/update/<int:pk>/', OrderItemInlineView.as_view(), name='order_items_update'),
    path('items/return/<int:pk>/', OrderItemReturnView.as_view(), name='order_items_return'),
    path('installment/creation/<int:pk>/', InstallmentCreationView.as_view(), name='installment_creation'),
]
