from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import reverse, redirect
from django.views.generic.edit import SingleObjectMixin
from django.views.generic import CreateView, DetailView, UpdateView, ListView, FormView

from cart.cart import Cart
from shop.models import Product
from cart.decorators import CartLenMixIn
from accounts.decorators import UserAuthMixIn
from utility_tools.views import BaseItemInlineView
from .models import Order, OrderItem, OrderItemReturn
from .decorators import OrderOwnerMixIn, OrderEditableMixin, OrderItemsLenMixin, OrderBalanceMixIn
from .forms import OrderCreateForm, InstallmentCreationForm, OrderItemInlineFormset, OrderItemReturnForm


class OrderCreationView(CartLenMixIn, UserAuthMixIn, OrderBalanceMixIn, CreateView):
    model = Order
    form_class = OrderCreateForm
    template_name = 'order/creation.html'

    def form_valid(self, form):
        form.instance.user = self.request.user
        self.object = form.save()
        self.create_order_items()
        if self.object.payment_method == 'installment':
            return redirect('order:installment_creation', self.object.pk)
        return HttpResponseRedirect(self.get_success_url())

    def create_order_items(self):
        cart_order = Cart(self.request)
        cart_content = cart_order.get_cart_content()
        for product_id, quantity in cart_content.items():
            product = Product.objects.filter(id=product_id)
            if not product.exists():
                continue
            product = product.first()
            if product.stock >= quantity:
                OrderItem.objects.create(
                    order=self.object, product=product, quantity=quantity
                )
            else:
                messages.warning(self.request, f"{product.name} has been dropped out of order because of lack of quantity")
        cart_order.clear()
        messages.success(self.request, 'Order has placed successfully')


class OrderDetailView(UserAuthMixIn, OrderOwnerMixIn, OrderItemsLenMixin, DetailView):
    model = Order
    template_name = 'order/detail.html'
    context_object_name = 'order'
    redirect_message = 'Order has no items'


class OrderUpdateView(UserAuthMixIn, OrderOwnerMixIn, OrderEditableMixin, UpdateView):
    model = Order
    form_class = OrderCreateForm
    template_name = 'order/update.html'


class OrderListView(UserAuthMixIn, ListView):
    model = Order
    ordering = ('created', )
    template_name = 'order/list.html'
    context_object_name = 'orders'

    def get_queryset(self):
        queryset = self.model.objects.filter(user=self.request.user)
        ordering = self.get_ordering()
        if ordering:
            if isinstance(ordering, str):
                ordering = (ordering,)
            queryset = queryset.order_by(*ordering)
        return queryset


class OrderItemInlineView(UserAuthMixIn, OrderOwnerMixIn, OrderEditableMixin, OrderItemsLenMixin, BaseItemInlineView):
    model = Order
    form_class = OrderItemInlineFormset
    template_name = 'order/order_item_inline.html'
    success_message = 'Order items has ben updated successfully'
    context_object_name = 'order_items'
    redirect_message = "Order has no items, all of them were returned"

    def get_success_url(self):
        return reverse('order:order_detail', kwargs={'pk': self.object.pk})


class OrderItemReturnView(UserAuthMixIn, SingleObjectMixin, OrderItemsLenMixin, FormView):

    model = Order
    form_class = OrderItemReturnForm
    template_name = 'order/order_item_return.html'
    success_message = 'Order items has ben removed successfully'
    context_object_name = 'order'
    redirect_message = 'Order has no items to be returned'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('order:order_detail', kwargs={'pk': self.object.pk})

    def get_initial(self):
        cpy = self.initial.copy()
        cpy.update({'queryset': self.object.items.filter(item_return__isnull=True)})
        return cpy

    def form_valid(self, form):
        items = form.cleaned_data.get('items')
        for item in items:
            OrderItemReturn.objects.create(order_item=item)
        return HttpResponseRedirect(self.get_success_url())


class InstallmentCreationView(SingleObjectMixin, OrderOwnerMixIn, FormView):
    model = Order
    form_class = InstallmentCreationForm
    template_name = 'order/installment_creation.html'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def get_initial(self):
        cpy = self.initial.copy()
        cpy.update({'order': self.object})
        return cpy

    def get_success_url(self):
        return reverse('order:order_detail', kwargs={'pk': self.object.pk})

    def form_valid(self, form):
        form.instance.order = self.object
        self.related_object = form.save()
        return HttpResponseRedirect(self.get_success_url())
