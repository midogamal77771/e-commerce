from django import forms
from django.forms.models import inlineformset_factory

from utility_tools.forms import BaseModelForm
from .models import Order, OrderItem, OrderItemReturn, Installment


class OrderCreateForm(BaseModelForm):

    class Meta:
        model = Order
        fields = ['payment_method', 'postal_code', 'location', 'more_description']


class OrderItemInlineForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['product'].disabled = True

    class Meta:
        model = OrderItem
        fields = ('product', 'quantity')


class OrderItemReturnInline(BaseModelForm):
    _return = forms.BooleanField(initial=False)

    class Meta:
        model = OrderItemReturn
        fields = ('order_item', )


class InstallmentCreationForm(BaseModelForm):
    readonly_fields = ('order', )

    class Meta:
        model = Installment
        fields = ('order', 'number_of_months')


class OrderItemReturnForm(forms.Form):
    items = forms.ModelMultipleChoiceField(queryset=OrderItem.objects.all(), widget=forms.CheckboxSelectMultiple())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields.get('items').queryset = self.initial.get('queryset')


OrderItemInlineFormset = inlineformset_factory(Order, OrderItem, form=OrderItemInlineForm, extra=0)
OrderItemReturnInlineFormset = inlineformset_factory(OrderItem, OrderItemReturn, form=OrderItemReturnInline, extra=0)
