from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Order, OrderItem, OrderItemReturn, Installment, InstallmentPay


class InstallmentPayInlineAdmin(admin.TabularInline):
    model = InstallmentPay
    can_delete = False
    raw_id_fields = ['installment']
    readonly_fields = ['month_number', 'pay_amount']


class InstallmentInlineAdmin(admin.TabularInline):
    model = Installment
    can_delete = False
    show_change_link = True
    raw_id_fields = ['order']
    readonly_fields = ['number_of_months', 'month_pay_amount']


class OrderItemInlineAdmin(admin.TabularInline):
    model = OrderItem
    raw_id_fields = ['product']
    readonly_fields = ['get_total_item_cost', 'show_image']

    def show_image(self, obj):
        return mark_safe(f"<a href='{obj.product.image.url}' ><img src='{obj.product.image.url}' width={130} height={170}></a>")


class OrderAdmin(admin.ModelAdmin):
    list_display = ['user', 'postal_code', 'delivered', 'delivered_date', 'created', 'id']
    fieldsets = (('Main info', {'fields': ('user', 'payment_method', 'postal_code', 'location', 'more_description')}),
                 ('Statues', {'fields': ('status', 'shipping_charge', 'get_total_order_cost', 'delivered', 'delivered_date')}),
                 )
    list_filter = ['created', 'updated']
    date_hierarchy = 'updated'
    readonly_fields = ['user', 'get_total_items_cost', "get_total_order_cost"]
    actions = ['get_out_order']
    ordering = ['-created']
    inlines = (OrderItemInlineAdmin, )

    def get_inlines(self, request, obj):
        fields = list(super().get_inlines(request, obj))
        if obj and obj.payment_method == 'installment':
            fields.append(InstallmentInlineAdmin)
        return fields

    def mark_as_shipped(self, request, queryset):
        query = queryset.update(shipped=True)
        self.message_user(request, f'{query.count} has changed successfully')

    mark_as_shipped.short_description = "mark as shipped"


class OrderItemAdmin(admin.ModelAdmin):
    list_display = ['product', 'quantity', 'created', 'updated', 'get_total_item_cost']


class OrderItemReturnAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'date', 'extra_fees')
    readonly_fields = ('order_item', )


class InstallmentAdmin(admin.ModelAdmin):
    inlines = (InstallmentPayInlineAdmin, )


admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem, OrderItemAdmin)
admin.site.register(Installment, InstallmentAdmin)
admin.site.register(OrderItemReturn, OrderItemReturnAdmin)
