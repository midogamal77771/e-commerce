from django.db.models import Manager, Case, When, Sum, Max, Count, F


class OrderItemManger(Manager):

    def all(self):
        return self.filter(product__isnull=False, order__isnull=False)

    def filter(self, *args, **kwargs):
        return super(OrderItemManger, self).filter(*args, **kwargs).filter(product__isnull=False, order__isnull=False)

    def get_most_order_items(self):
        self.annotate(ordered=Count('product') * F('quantity'))

    def group_by(self, value: str, order_by: str):
        return self.values(value).annotate(
            actual_price=Case(When(product__price_sale__isnull=False, then=F('product__price_sale')), default=F('product__price')),
            total_price=Sum(F('product__price') * F('quantity'))
        ).order_by(order_by)

    def return_group_by(self, value: str, order_by: str):
        return self.filter(item_return__isnull=False).values(value).annotate(
            actual_price=Case(When(product__price_sale__isnull=False, then=F('product__price_sale')), default=F('product__price')),
            total_price=Sum(F('product__price') * F('quantity'))
        ).order_by(order_by)
