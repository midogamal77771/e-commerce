from django.contrib import messages
from django.shortcuts import redirect
from django.contrib.auth.mixins import AccessMixin

from .models import Order


class OrderOwnerMixIn(AccessMixin):
    redirect_url: str = 'order:order_list'

    def dispatch(self, *args, **kwargs):
        if self.get_object().user == self.request.user:
            return super().dispatch(*args, **kwargs)
        return redirect(self.redirect_url)


class OrderEditableMixin(AccessMixin):
    redirect_url: str = 'order:order_list'

    def dispatch(self, request, *args, **kwargs):
        if self.get_object().available_by_status() and self.get_object().available_by_date():
            return super().dispatch(request, *args, **kwargs)
        messages.error(request, "Order is not editable")
        return redirect(self.redirect_url)


class OrderItemsLenMixin(AccessMixin):
    redirect_url: str = 'order:order_list'
    redirect_message: str = "No access to order"

    def dispatch(self, request, *args, **kwargs):
        if len(self.get_object().get_proper_items()):
            return super().dispatch(request, *args, **kwargs)
        messages.error(request, self.redirect_message)
        return redirect(self.redirect_url)


class OrderBalanceMixIn(AccessMixin):
    redirect_url: str = 'order:order_list'

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        orders = Order.objects.filter(user=user)
        nums = sum(order.installment.get_reminder_installment_pay_count() for order in orders
                   if hasattr(order, 'installment'))
        if nums == 0:
            return super().dispatch(request, *args, **kwargs)
        messages.error(request, 'You have un balanced order, finish it first to be able to order new items')
        return redirect(self.redirect_url)
