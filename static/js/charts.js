function randomRgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(1) + ')';
};

function getUrl(element_id){
    return document.location.origin + JSON.parse(document.getElementById(element_id).textContent)
}

function plotSingleChart(element_id, chart_type, data_labels, datasets){
    let ctx = document.getElementById(element_id).getContext('2d');
    let myChart = new Chart(ctx, {
        type: chart_type,
        data: {
            labels: data_labels,
            datasets: [datasets, ]
        },
    });
};


function plotMultiChart(element_id, data_labels, dataset1, dataset2){
    let ctx = document.getElementById(element_id).getContext('2d');
    let myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: data_labels,
            datasets: [dataset1, dataset2]
        },
    });
};


function callAjaxSinglePlot(url, element_id, chart_type, chart_label, property_label, data_label){
    $.ajax({
        url: url,
        method:'GET',
        success: function(data){
            let data_labels = data.map( e => e[property_label]);
            let data_numeric = data.map( e => Number(e[data_label]));
            let colors =  data.map( e => randomRgba());
            let datasets = {
                label:chart_label,
                data: data_numeric,
                backgroundColor: colors,
                borderColor: colors,
                borderWidth: 1,
            };
            plotSingleChart(element_id, chart_type, data_labels, datasets);
        },
    })
};


function callAjaxMultiPlot(url, element_id, chart_label, property_label, data_label){
    $.ajax({
        url: url,
        method:'GET',
        success: function(data){

            let data_labels1 = data.sales.map( e => e[property_label]);
            let data_numeric1 = data.sales.map( e => Number(e[data_label]));
            let colors1 =  ['rgb(54, 162, 235)'];
            let dataset1 = {
                label:'Sales Curve',
                data: data_numeric1,
                backgroundColor: colors1,
                borderColor: colors1,
                borderWidth: 1,
            };

            let data_labels2 = data.returns.map( e => e[property_label]);
            let data_numeric2 = data.returns.map( e => Number(e[data_label]));
            let colors2 = ['rgb(255, 99, 132)'] ;
            let dataset2 = {
                label:'Returns Curve',
                data: data_numeric2,
                backgroundColor: colors2,
                borderColor: colors2,
                borderWidth: 1,
            };

            plotMultiChart(element_id, data_labels1, dataset1, dataset2);
        },
    })
};


callAjaxMultiPlot(
    url=getUrl('monthly_sales_url'),
    element_id="sales-curve",
    chart_label="Sales Curve",
    property_label="order__created__date",
    data_label="total_price"
    );
callAjaxSinglePlot(
    url=getUrl('sub_category_sales_url'),
    element_id="sub-category-percentage",
    chart_type="polarArea",
    chart_label="Sub Category Percentage",
    property_label="product__sub_category__name",
    data_label="total_price"
    );
callAjaxSinglePlot(
    url=getUrl('main_category_sales_url'),
    element_id="main-category-percentage",
    chart_type="doughnut",
    chart_label="Main Category Percentage",
    property_label="product__sub_category__main_category__name",
    data_label="total_price"
    );

