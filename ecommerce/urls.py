from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('shop.urls', namespace='shop')),
    path('', include('accounts.urls', namespace='accounts')),
    path('info/', include('info.urls', namespace='info')),
    path('cart/', include('cart.urls', namespace='cart')),
    path('wish/', include('wish.urls', namespace='wish')),
    path('order/', include('order.urls', namespace='order')),
    path('data/', include('data_processing.urls', namespace='data_processing')),
]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
